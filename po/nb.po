# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Allan Nordhøy <epost@anotheragency.no>, 2014-2015
# Allan Nordhøy <epost@anotheragency.no>, 2016
# Harald H. <haarektrans@gmail.com>, 2014
# Kjell Cato Heskjestad <cato@heskjestad.xyz>, 2019-2020
# Terje Uriansrud <ter@operamail.com>, 2007
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-04-02 00:31+0200\n"
"PO-Revision-Date: 2020-04-04 12:16+0000\n"
"Last-Translator: Kjell Cato Heskjestad <cato@heskjestad.xyz>\n"
"Language-Team: Norwegian Bokmål (http://www.transifex.com/xfce/xfce-panel-plugins/language/nb/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: nb\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../data/appdata/xfce4-clipman.appdata.xml.in.h:1
msgid ""
"Clipboard Manager provided as a panel plugin for Xfce and as a standalone "
"application running in the notification area. It keeps a history of text and"
" images of content copied to the clipboard. It also has a feature to execute"
" actions on specific text selection by matching them against regexes."
msgstr "Utklippstavlebehandleren tilbys som et paneltillegg for Xfce og som frittstående program kjørende i varslingsområdet. Det tar vare på historikk og tekst fra bilder og innhold som kopieres til utklippstavlen. Det har også den egenskapen at det utfører handlinger på gitt tekst ved å jamføre dem mot regulære uttrykk."

#: ../panel-plugin/xfce4-clipman.desktop.in.h:1
msgid "Clipboard Manager"
msgstr "Utklippsbehandler"

#: ../panel-plugin/xfce4-clipman.desktop.in.h:2
msgid "Clipboard Manager Autostart File"
msgstr "Autostartfil for utklippsbehandler"

#: ../panel-plugin/xfce4-clipman-actions.xml.in.h:1
msgid "Image"
msgstr "Bilde"

#: ../panel-plugin/xfce4-clipman-actions.xml.in.h:2
msgid "Edit with Gimp"
msgstr "Rediger med Gimp"

#: ../panel-plugin/xfce4-clipman-actions.xml.in.h:3
msgid "View with Ristretto"
msgstr "Vis i Ristretto"

#: ../panel-plugin/xfce4-clipman-actions.xml.in.h:4
msgid "Bugz"
msgstr "Bugz"

#: ../panel-plugin/xfce4-clipman-actions.xml.in.h:5
msgid "Xfce Bug"
msgstr "Xfce-feil"

#: ../panel-plugin/xfce4-clipman-actions.xml.in.h:6
msgid "GNOME Bug"
msgstr "GNOME-feil"

#: ../panel-plugin/xfce4-clipman-actions.xml.in.h:7
msgid "Long URL"
msgstr "Lang URL"

#: ../panel-plugin/xfce4-clipman-actions.xml.in.h:8
msgid "Shrink the URL"
msgstr "Forkort URL"

#: ../panel-plugin/xfce4-clipman-plugin.desktop.in.h:1
#: ../panel-plugin/xfce4-clipman-plugin-autostart.desktop.in.h:1
#: ../panel-plugin/main-panel-plugin.c:77
#: ../panel-plugin/main-status-icon.c:81 ../panel-plugin/plugin.c:95
#: ../panel-plugin/plugin.c:333
msgid "Clipman"
msgstr "Clipman"

#: ../panel-plugin/xfce4-clipman-plugin.desktop.in.h:2
#: ../panel-plugin/xfce4-clipman-plugin-autostart.desktop.in.h:2
msgid "Clipboard manager"
msgstr "Utklippsbehandler"

#: ../panel-plugin/xfce4-clipman-settings.c:97
msgid "None"
msgstr "Ingen"

#. TRANSLATORS: Keyboard shortcut
#: ../panel-plugin/xfce4-clipman-settings.c:99
msgid "Ctrl+V"
msgstr "Ctrl + V"

#. TRANSLATORS: Keyboard shortcut
#: ../panel-plugin/xfce4-clipman-settings.c:101
msgid "Shift+Insert"
msgstr "Shift + Insert"

#: ../panel-plugin/xfce4-clipman-settings.c:535
msgid "<b>Reset actions</b>"
msgstr "<b>Tilbakestill handlinger</b>"

#: ../panel-plugin/xfce4-clipman-settings.c:537
msgid ""
"Are you sure you want to reset the actions to the system default values?"
msgstr "Er du sikker på at du ønsker å tilbakestille handlingene til standard systemverdier?"

#: ../panel-plugin/settings-dialog.ui.h:1
#: ../panel-plugin/xfce4-clipman-history.c:450
#: ../panel-plugin/xfce4-clipman-history.c:452
msgid "_Help"
msgstr "_Hjelp"

#: ../panel-plugin/settings-dialog.ui.h:2
msgid "_Close"
msgstr "Lukk"

#: ../panel-plugin/settings-dialog.ui.h:3
msgid "Sync mouse _selections"
msgstr "Oppdater _museutvalg"

#: ../panel-plugin/settings-dialog.ui.h:4
msgid ""
"If checked, the selections will be synced with the default clipboard in a "
"way that you can paste what you select"
msgstr "Hvis huket av, vil måten utvalgene synkroniseres med forvalgt utklippstavle håndteres slik at du kan lime inn det du velger"

#: ../panel-plugin/settings-dialog.ui.h:5
msgid "_QR-Code support"
msgstr "Støtte for _QR-kode"

#: ../panel-plugin/settings-dialog.ui.h:6
msgid ""
"If checked, the menu shows a QR-Code of the currently selected clipboard "
"entry"
msgstr "Hvis avhuket, vil menyen vise en QR-kode for valgt utklipp"

#: ../panel-plugin/settings-dialog.ui.h:7
msgid "Automatically paste a selected item from the history"
msgstr "Automatisk lim inn valgt element fra historikken."

#: ../panel-plugin/settings-dialog.ui.h:8
msgid "_Paste instantly:"
msgstr "_Lim inn umiddelbart:"

#: ../panel-plugin/settings-dialog.ui.h:9
msgid "<b>General</b>"
msgstr "<b>Generelt</b>"

#: ../panel-plugin/settings-dialog.ui.h:10
msgid "P_osition menu at mouse pointer"
msgstr "P_osisjoner meny på peker"

#: ../panel-plugin/settings-dialog.ui.h:11
msgid ""
"Popup the menu at the mouse pointer position, only for the xfce4-popup-"
"clipman command"
msgstr "Oppsprettsmeny på pekerposisjonen, bare for xfce4-popup-clipman kommando"

#: ../panel-plugin/settings-dialog.ui.h:12
msgid "Maximum items:"
msgstr "Største antall elementer:"

#: ../panel-plugin/settings-dialog.ui.h:13
msgid "<b>Menu</b>"
msgstr "<b>Meny</b>"

#: ../panel-plugin/settings-dialog.ui.h:14
msgid "_Behavior"
msgstr "_Oppførsel"

#: ../panel-plugin/settings-dialog.ui.h:15
msgid "<b>_Enable automatic actions</b>"
msgstr "<b>_Slå på automatiske handlinger</b>"

#: ../panel-plugin/settings-dialog.ui.h:16
msgid ""
"If checked, the clipboard texts will be matched against regular expressions and a menu will display possible actions automatically.\n"
"Otherwise the menu will only appear when calling \"xfce4-popup-clipman-actions\" (and a match was found)"
msgstr "Hvis huket av så vil tekst i utklippstavlen bli jamført med regulære uttrykk og en meny vil vise mulige handlinger automatisk.\nEllers vil menyen dukke opp hvis man påkaller «xfce4-popup-clipman-actions» (og noe som samsvarer ble funnet)"

#: ../panel-plugin/settings-dialog.ui.h:18
msgid "_Show actions by holding Control"
msgstr "_Vis handlinger ved å holde nede Ctrl-tasten"

#: ../panel-plugin/settings-dialog.ui.h:19
msgid ""
"When the selection is done (mouse or keyboard) and the Control key is still "
"pressed down, the popup menu for matched actions will be shown"
msgstr "Når et utvalget er gjort ( med mus eller tastatur) og Ctrl-tasten fremdeles er nede, vil oppsprettsmenyen for jamførte handlinger bli vist"

#: ../panel-plugin/settings-dialog.ui.h:20
msgid "S_kip actions by holding Control"
msgstr "H_opp over handlinger ved å holde nede Ctrl"

#: ../panel-plugin/settings-dialog.ui.h:21
msgid ""
"When the selection is done (mouse or keyboard) and the Control key is still "
"pressed down, the popup menu for matched actions will be skipped"
msgstr "Når et utvalget er gjort (med mus eller tastatur) og Ctrl-tasten fremdeles er nede, vil oppsprettsmenyen for jamførte handlinger utgå"

#: ../panel-plugin/settings-dialog.ui.h:22
msgid "Add action"
msgstr "Legg til handling"

#: ../panel-plugin/settings-dialog.ui.h:23
msgid "Edit action"
msgstr "Rediger handling"

#: ../panel-plugin/settings-dialog.ui.h:24
msgid "Delete action"
msgstr "Slett handling"

#: ../panel-plugin/settings-dialog.ui.h:25
msgid "Reset all actions to the system default values"
msgstr "Tilbakestill alle handlinger til systemforvalgte verdier"

#: ../panel-plugin/settings-dialog.ui.h:26
msgid "_Actions"
msgstr "_Handlinger"

#: ../panel-plugin/settings-dialog.ui.h:27
msgid "<b>Remember history</b>"
msgstr "<b>Husk historikk</b>"

#: ../panel-plugin/settings-dialog.ui.h:28
msgid "Remember last copied _image"
msgstr "Husk sist kopierte _bilde"

#: ../panel-plugin/settings-dialog.ui.h:29
msgid "If checked, this option allows to store one image inside the history"
msgstr "Hvis huket av så vil dette valget tillate deg å lagre ett bilde i historikken"

#: ../panel-plugin/settings-dialog.ui.h:30
msgid "_Reorder history items"
msgstr "_Reorganiser historikkelementer"

#: ../panel-plugin/settings-dialog.ui.h:31
msgid ""
"Push last copied text to the top of the history, useful to reorder old items"
msgstr "Puff sist kopierte tekst til toppen i historikken, nyttig for å reorganisere gamle elementer"

#: ../panel-plugin/settings-dialog.ui.h:32
msgid "Re_verse history order"
msgstr "Re_verser historikkrekkefølgen"

#: ../panel-plugin/settings-dialog.ui.h:33
msgid "Reverse order of the history shown in the menu"
msgstr "Reverser historikkrekkefølgen vist i menyen"

#: ../panel-plugin/settings-dialog.ui.h:34
msgid "Ignore mouse s_elections"
msgstr "Ignorer m_useutvalg"

#: ../panel-plugin/settings-dialog.ui.h:35
msgid ""
"If checked, the selections won't affect the history except the manual copies"
msgstr "Hvis huket av så vil utvalgene ikke ha innvirkning på historikken, bortsett manuelle kopier"

#: ../panel-plugin/settings-dialog.ui.h:36
msgid "Size of the _history:"
msgstr "Størrelse på _historikken:"

#: ../panel-plugin/settings-dialog.ui.h:37
msgid "5"
msgstr "5"

#: ../panel-plugin/settings-dialog.ui.h:38
msgid "H_istory"
msgstr "H_istorikk"

#: ../panel-plugin/settings-dialog.ui.h:39
msgid "Edit Action"
msgstr "Rediger handling"

#: ../panel-plugin/settings-dialog.ui.h:40
msgid "Name:"
msgstr "Navn:"

#: ../panel-plugin/settings-dialog.ui.h:41
msgid "Pattern:"
msgstr "Mønster:"

#: ../panel-plugin/settings-dialog.ui.h:42
msgid ""
"You can use the substitution parameters \"\\1\", \"\\2\" and so on in the "
"commands. The parameter \"\\0\" represents the complete text. The pattern is"
" always anchored within the special characters ^$"
msgstr "Man kan bruke alternative parametere «\\1», «\\2» osv. i kommandoene. Parameteren «\\0» står for hele teksten. Mønsteret forankres mellom spesialtegnene ^$."

#: ../panel-plugin/settings-dialog.ui.h:43
msgid "Activate only on manual copy"
msgstr "Aktiver kun ved manuell kopiering"

#: ../panel-plugin/settings-dialog.ui.h:44
msgid ""
"By default the action is triggerred by a selection, check this option to "
"trigger the action only when you make a manual copy"
msgstr "Forvalgt handlingsmønster er at handling blir utløst av et utvalg, huk av dette valget for å sette i verk en handling bare når du tar en kopi manuelt"

#: ../panel-plugin/settings-dialog.ui.h:45
msgid "<b>Action</b>"
msgstr "<b>Handling</b>"

#: ../panel-plugin/settings-dialog.ui.h:46
msgid "Command:"
msgstr "Kommando:"

#: ../panel-plugin/settings-dialog.ui.h:47
msgid "<b>Commands</b>"
msgstr "<b>Kommandoer</b>"

#: ../panel-plugin/settings-dialog.ui.h:48
msgid "Type here your custom text, for example a URL, a filename, etc."
msgstr "Skriv inn selvvalgt tekst, for eksempel en adresse, et filnavn eller lignende."

#: ../panel-plugin/settings-dialog.ui.h:49
msgid "Regular expression"
msgstr "Regulært uttrykk"

#: ../panel-plugin/main-panel-plugin.c:111
#: ../panel-plugin/main-status-icon.c:159
msgid "_Disable"
msgstr "_Deaktiver"

#: ../panel-plugin/actions.c:368
#, c-format
msgid ""
"Unable to execute the command \"%s\"\n"
"\n"
"%s"
msgstr "Kan ikke utføre kommandoen «%s»\n\n%s"

#: ../panel-plugin/menu.c:246
msgid "Are you sure you want to clear the history?"
msgstr "Er du sikker at du vil slette historikken?"

#: ../panel-plugin/menu.c:251
msgid "Don't ask again"
msgstr "Ikke spør igjen"

#: ../panel-plugin/menu.c:297
msgid "Unable to open the clipman history dialog"
msgstr "Klarte ikke åpne historikkvinduet"

#: ../panel-plugin/menu.c:411
msgid "Could not generate QR-Code."
msgstr "Klarte ikke å generere QR-kode."

#. Insert empty menu item
#: ../panel-plugin/menu.c:425 ../panel-plugin/xfce4-clipman-history.c:327
msgid "Clipboard is empty"
msgstr "Utklippstavlen er tom"

#: ../panel-plugin/menu.c:534
msgid "_Show full history..."
msgstr "_Vis full historikk …"

#: ../panel-plugin/menu.c:540
msgid "_Clear history"
msgstr "_Tøm historikk"

#: ../panel-plugin/menu.c:547
msgid "_Clipman settings..."
msgstr "_Clipmans innstillinger …"

#: ../panel-plugin/plugin.c:321
msgid "Contributors:"
msgstr "Medvirkende:"

#: ../panel-plugin/plugin.c:335
msgid "Clipboard Manager for Xfce"
msgstr "Utklippstavlebehandler for Xfce"

#: ../panel-plugin/plugin.c:343
msgid "translator-credits"
msgstr "haarek - Harald <https://www.transifex.com/accounts/profile/haarek/>\nkingu - Allan Nordhøy <https://www.transifex.com/accounts/profile/kingu/>\npapparonny - Ronny K. M. Olufsen <https://www.transifex.com/accounts/profile/papparonny/>\nTerje Uriansrud <terje@uriansrud.net>"

#: ../panel-plugin/plugin.c:357
msgid "Unable to open the settings dialog"
msgstr "Klarte ikke åpne innstillingsdialog"

#: ../panel-plugin/common.c:29
msgid ""
"Could not start the Clipboard Manager Daemon because it is already running."
msgstr "Klarte ikke starte utklippstavlebehandlerens nisse fordi den allerede kjører."

#: ../panel-plugin/common.c:30
msgid "The Xfce Clipboard Manager is already running."
msgstr "Xfce Utklippstavlebehandler kjører allerede."

#: ../panel-plugin/common.c:37
msgid "You can launch it with 'xfce4-clipman'."
msgstr "Den kan startes med «xfce4-clipman»."

#: ../panel-plugin/common.c:39
msgid "The Clipboard Manager Daemon is not running."
msgstr "Utklippstavlebehandlerens nisse kjører ikke."

#: ../panel-plugin/xfce4-clipman-history.c:249
msgid "Enter search phrase here"
msgstr "Skriv inn søketekst her"

#: ../panel-plugin/xfce4-clipman-history.c:423
#, c-format
msgid "_Paste"
msgstr "_Lim inn"

#: ../panel-plugin/xfce4-clipman-history.c:428
#, c-format
msgid "_Copy"
msgstr "_Kopier"

#: ../panel-plugin/xfce4-clipman-history.c:444
msgid "Clipman History"
msgstr "Historikk"

#: ../panel-plugin/xfce4-clipman-history.c:458
#: ../panel-plugin/xfce4-clipman-history.c:460
msgid "_Settings"
msgstr "_Innstillinger"

#: ../panel-plugin/xfce4-clipman-settings.desktop.in.h:1
msgid "Clipboard Manager Settings"
msgstr "Utklippstavlebehandlerens innstillinger"

#: ../panel-plugin/xfce4-clipman-settings.desktop.in.h:2
msgid "Customize your clipboard"
msgstr "Tilpass utklippstavla"
