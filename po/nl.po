# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Pjotr <pjotrvertaalt@gmail.com>, 2014,2016-2017,2020
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-04-02 00:31+0200\n"
"PO-Revision-Date: 2020-04-03 15:30+0000\n"
"Last-Translator: Pjotr <pjotrvertaalt@gmail.com>\n"
"Language-Team: Dutch (http://www.transifex.com/xfce/xfce-panel-plugins/language/nl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: nl\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../data/appdata/xfce4-clipman.appdata.xml.in.h:1
msgid ""
"Clipboard Manager provided as a panel plugin for Xfce and as a standalone "
"application running in the notification area. It keeps a history of text and"
" images of content copied to the clipboard. It also has a feature to execute"
" actions on specific text selection by matching them against regexes."
msgstr "Klembordbeheer als invoegsel voor de werkbalk van Xfce en als een op zichzelf staande toepassing die draait in het meldvak. Hij houdt een geschiedenis bij van tekst en afbeeldingen die zijn gekopieerd naar het klembord. Hij heeft ook een functie waarmee acties kunnen worden uitgevoerd op een bepaalde tekstselectie, door deze af te zetten tegen regexes."

#: ../panel-plugin/xfce4-clipman.desktop.in.h:1
msgid "Clipboard Manager"
msgstr "Klembordbeheer"

#: ../panel-plugin/xfce4-clipman.desktop.in.h:2
msgid "Clipboard Manager Autostart File"
msgstr "Bestand voor automatische start van Klembordbeheer"

#: ../panel-plugin/xfce4-clipman-actions.xml.in.h:1
msgid "Image"
msgstr "Afbeelding"

#: ../panel-plugin/xfce4-clipman-actions.xml.in.h:2
msgid "Edit with Gimp"
msgstr "Bewerken met Gimp"

#: ../panel-plugin/xfce4-clipman-actions.xml.in.h:3
msgid "View with Ristretto"
msgstr "Weergeven in Ristretto"

#: ../panel-plugin/xfce4-clipman-actions.xml.in.h:4
msgid "Bugz"
msgstr "Bugz"

#: ../panel-plugin/xfce4-clipman-actions.xml.in.h:5
msgid "Xfce Bug"
msgstr "Xfce-fout"

#: ../panel-plugin/xfce4-clipman-actions.xml.in.h:6
msgid "GNOME Bug"
msgstr "GNOME-fout"

#: ../panel-plugin/xfce4-clipman-actions.xml.in.h:7
msgid "Long URL"
msgstr "Lange URL"

#: ../panel-plugin/xfce4-clipman-actions.xml.in.h:8
msgid "Shrink the URL"
msgstr "Webadres inkrimpen"

#: ../panel-plugin/xfce4-clipman-plugin.desktop.in.h:1
#: ../panel-plugin/xfce4-clipman-plugin-autostart.desktop.in.h:1
#: ../panel-plugin/main-panel-plugin.c:77
#: ../panel-plugin/main-status-icon.c:81 ../panel-plugin/plugin.c:95
#: ../panel-plugin/plugin.c:333
msgid "Clipman"
msgstr "Clipman"

#: ../panel-plugin/xfce4-clipman-plugin.desktop.in.h:2
#: ../panel-plugin/xfce4-clipman-plugin-autostart.desktop.in.h:2
msgid "Clipboard manager"
msgstr "Klembordbeheer"

#: ../panel-plugin/xfce4-clipman-settings.c:97
msgid "None"
msgstr "Geen"

#. TRANSLATORS: Keyboard shortcut
#: ../panel-plugin/xfce4-clipman-settings.c:99
msgid "Ctrl+V"
msgstr "Ctrl+V"

#. TRANSLATORS: Keyboard shortcut
#: ../panel-plugin/xfce4-clipman-settings.c:101
msgid "Shift+Insert"
msgstr "Shift+Insert"

#: ../panel-plugin/xfce4-clipman-settings.c:535
msgid "<b>Reset actions</b>"
msgstr "<b>Acties op beginwaarden instellen</b>"

#: ../panel-plugin/xfce4-clipman-settings.c:537
msgid ""
"Are you sure you want to reset the actions to the system default values?"
msgstr "Weet u zeker dat u de acties wilt terugzetten op de standaardwaarden?"

#: ../panel-plugin/settings-dialog.ui.h:1
#: ../panel-plugin/xfce4-clipman-history.c:450
#: ../panel-plugin/xfce4-clipman-history.c:452
msgid "_Help"
msgstr "_Hulp"

#: ../panel-plugin/settings-dialog.ui.h:2
msgid "_Close"
msgstr "S_luiten"

#: ../panel-plugin/settings-dialog.ui.h:3
msgid "Sync mouse _selections"
msgstr "Muisselecties _synchroniseren"

#: ../panel-plugin/settings-dialog.ui.h:4
msgid ""
"If checked, the selections will be synced with the default clipboard in a "
"way that you can paste what you select"
msgstr "Met deze optie zullen selecties op zo'n manier gesynchroniseerd worden met het standaardklembord, dat u kunt plakken wat u selecteert"

#: ../panel-plugin/settings-dialog.ui.h:5
msgid "_QR-Code support"
msgstr "Ondersteuning voor _QR-code"

#: ../panel-plugin/settings-dialog.ui.h:6
msgid ""
"If checked, the menu shows a QR-Code of the currently selected clipboard "
"entry"
msgstr "Indien aangevinkt toont het menu een QR-code van het thans geselecteerde element in het klembord"

#: ../panel-plugin/settings-dialog.ui.h:7
msgid "Automatically paste a selected item from the history"
msgstr "Plak automatisch een geselecteerd element vanuit de geschiedenis"

#: ../panel-plugin/settings-dialog.ui.h:8
msgid "_Paste instantly:"
msgstr "_Plak meteen:"

#: ../panel-plugin/settings-dialog.ui.h:9
msgid "<b>General</b>"
msgstr "<b>Algemeen</b>"

#: ../panel-plugin/settings-dialog.ui.h:10
msgid "P_osition menu at mouse pointer"
msgstr "Plaats menu bij de muispijl"

#: ../panel-plugin/settings-dialog.ui.h:11
msgid ""
"Popup the menu at the mouse pointer position, only for the xfce4-popup-"
"clipman command"
msgstr "Laat het menu opduiken bij de muispijlpositie, maar alleen voor de opdracht xfce4-popup-clipman"

#: ../panel-plugin/settings-dialog.ui.h:12
msgid "Maximum items:"
msgstr "Maximaal aantal elementen:"

#: ../panel-plugin/settings-dialog.ui.h:13
msgid "<b>Menu</b>"
msgstr "<b>Menu</b>"

#: ../panel-plugin/settings-dialog.ui.h:14
msgid "_Behavior"
msgstr "_Gedrag"

#: ../panel-plugin/settings-dialog.ui.h:15
msgid "<b>_Enable automatic actions</b>"
msgstr "<b>_Automatische acties inschakelen</b>"

#: ../panel-plugin/settings-dialog.ui.h:16
msgid ""
"If checked, the clipboard texts will be matched against regular expressions and a menu will display possible actions automatically.\n"
"Otherwise the menu will only appear when calling \"xfce4-popup-clipman-actions\" (and a match was found)"
msgstr "Indien aangevinkt, zullen de klembordteksten worden vergeleken met overeenkomstige reguliere uitdrukkingen en een menu zal automatisch mogelijke acties tonen.\nAnders zal het menu alleen verschijnen bij het aanroepen van 'xfce4-popup-clipman-actions' (als er een overeenkomst werd gevonden)"

#: ../panel-plugin/settings-dialog.ui.h:18
msgid "_Show actions by holding Control"
msgstr "Toon acties door de Ctrl-toets ingedrukt te houden"

#: ../panel-plugin/settings-dialog.ui.h:19
msgid ""
"When the selection is done (mouse or keyboard) and the Control key is still "
"pressed down, the popup menu for matched actions will be shown"
msgstr "Wanneer de selectie is voltooid (met muis of toetsenbord) en de Ctrl-toets nog steeds ingedrukt is, zal het opduikmenu voor overeenkomende acties worden getoond"

#: ../panel-plugin/settings-dialog.ui.h:20
msgid "S_kip actions by holding Control"
msgstr "Sla acties door Ctrl-toets ingedrukt te houden, over"

#: ../panel-plugin/settings-dialog.ui.h:21
msgid ""
"When the selection is done (mouse or keyboard) and the Control key is still "
"pressed down, the popup menu for matched actions will be skipped"
msgstr "Wanneer de selectie gereed is (muis of toetsenbord) en de Ctrl-toets nog steeds ingedrukt is, zal het opduikmenu voor overeenkomende acties worden overgeslagen"

#: ../panel-plugin/settings-dialog.ui.h:22
msgid "Add action"
msgstr "Actie toevoegen"

#: ../panel-plugin/settings-dialog.ui.h:23
msgid "Edit action"
msgstr "Actie bewerken"

#: ../panel-plugin/settings-dialog.ui.h:24
msgid "Delete action"
msgstr "Actie wissen"

#: ../panel-plugin/settings-dialog.ui.h:25
msgid "Reset all actions to the system default values"
msgstr "Alle acties terugzetten op de standaardwaarden van het systeem"

#: ../panel-plugin/settings-dialog.ui.h:26
msgid "_Actions"
msgstr "_Acties"

#: ../panel-plugin/settings-dialog.ui.h:27
msgid "<b>Remember history</b>"
msgstr "<b>Geschiedenis onthouden</b>"

#: ../panel-plugin/settings-dialog.ui.h:28
msgid "Remember last copied _image"
msgstr "Onthoud laatst gekopieerde afbeelding"

#: ../panel-plugin/settings-dialog.ui.h:29
msgid "If checked, this option allows to store one image inside the history"
msgstr "Deze optie biedt de mogelijkheid één afbeelding in de geschiedenis op te slaan"

#: ../panel-plugin/settings-dialog.ui.h:30
msgid "_Reorder history items"
msgstr "Elementen van de klembordgeschiedenis herschikken"

#: ../panel-plugin/settings-dialog.ui.h:31
msgid ""
"Push last copied text to the top of the history, useful to reorder old items"
msgstr "Plaats de laatst gekopieerde tekst bovenaan in de geschiedenis, voor het gemak"

#: ../panel-plugin/settings-dialog.ui.h:32
msgid "Re_verse history order"
msgstr "Keer de geschiedenisvolgorde om"

#: ../panel-plugin/settings-dialog.ui.h:33
msgid "Reverse order of the history shown in the menu"
msgstr "Draai de volgorde om van de geschiedenis zoals getoond in het menu"

#: ../panel-plugin/settings-dialog.ui.h:34
msgid "Ignore mouse s_elections"
msgstr "Muisselecties negeren"

#: ../panel-plugin/settings-dialog.ui.h:35
msgid ""
"If checked, the selections won't affect the history except the manual copies"
msgstr "Met deze optie zal de geschiedenis alleen veranderd worden bij handmatig kopiëren"

#: ../panel-plugin/settings-dialog.ui.h:36
msgid "Size of the _history:"
msgstr "Grootte van de _geschiedenis"

#: ../panel-plugin/settings-dialog.ui.h:37
msgid "5"
msgstr "5"

#: ../panel-plugin/settings-dialog.ui.h:38
msgid "H_istory"
msgstr "Geschiedenis"

#: ../panel-plugin/settings-dialog.ui.h:39
msgid "Edit Action"
msgstr "Actie bewerken"

#: ../panel-plugin/settings-dialog.ui.h:40
msgid "Name:"
msgstr "Naam:"

#: ../panel-plugin/settings-dialog.ui.h:41
msgid "Pattern:"
msgstr "Patroon:"

#: ../panel-plugin/settings-dialog.ui.h:42
msgid ""
"You can use the substitution parameters \"\\1\", \"\\2\" and so on in the "
"commands. The parameter \"\\0\" represents the complete text. The pattern is"
" always anchored within the special characters ^$"
msgstr "U kunt de vervangingsparameters \"\\1\", \"\\2\" enzovoorts gebruiken in de opdrachten. De parameter \"\\0\" staat voor de volledige tekst. Het patroon is altijd verankerd binnen de speciale tekens ^$"

#: ../panel-plugin/settings-dialog.ui.h:43
msgid "Activate only on manual copy"
msgstr "Alleen activeren bij handmatig kopiëren"

#: ../panel-plugin/settings-dialog.ui.h:44
msgid ""
"By default the action is triggerred by a selection, check this option to "
"trigger the action only when you make a manual copy"
msgstr "Standaard wordt de actie geactiveerd door een selectie; kies deze optie om de actie alleen te activeren wanneer er iets handmatig wordt gekopieerd"

#: ../panel-plugin/settings-dialog.ui.h:45
msgid "<b>Action</b>"
msgstr "<b>Actie</b>"

#: ../panel-plugin/settings-dialog.ui.h:46
msgid "Command:"
msgstr "Opdracht:"

#: ../panel-plugin/settings-dialog.ui.h:47
msgid "<b>Commands</b>"
msgstr "<b>Opdrachten</b>"

#: ../panel-plugin/settings-dialog.ui.h:48
msgid "Type here your custom text, for example a URL, a filename, etc."
msgstr "Tik hier uw eigen tekst, bijvoorbeeld een webadres of een bestandsnaam."

#: ../panel-plugin/settings-dialog.ui.h:49
msgid "Regular expression"
msgstr "Reguliere uitdrukking"

#: ../panel-plugin/main-panel-plugin.c:111
#: ../panel-plugin/main-status-icon.c:159
msgid "_Disable"
msgstr "Uitschakelen"

#: ../panel-plugin/actions.c:368
#, c-format
msgid ""
"Unable to execute the command \"%s\"\n"
"\n"
"%s"
msgstr "Kan de opdracht '%s' niet uitvoeren\n\n%s"

#: ../panel-plugin/menu.c:246
msgid "Are you sure you want to clear the history?"
msgstr "Weet u zeker dat u de geschiedenis wil wissen?"

#: ../panel-plugin/menu.c:251
msgid "Don't ask again"
msgstr "Vraag dit niet opnieuw"

#: ../panel-plugin/menu.c:297
msgid "Unable to open the clipman history dialog"
msgstr "Kan het dialoogvenster voor de geschiedenis van Klembordbeheer niet openen"

#: ../panel-plugin/menu.c:411
msgid "Could not generate QR-Code."
msgstr "Kon geen QR-code genereren."

#. Insert empty menu item
#: ../panel-plugin/menu.c:425 ../panel-plugin/xfce4-clipman-history.c:327
msgid "Clipboard is empty"
msgstr "Klembord is leeg"

#: ../panel-plugin/menu.c:534
msgid "_Show full history..."
msgstr "Toon volledige geschiedenis..."

#: ../panel-plugin/menu.c:540
msgid "_Clear history"
msgstr "Geschiedenis _wissen"

#: ../panel-plugin/menu.c:547
msgid "_Clipman settings..."
msgstr "Instellingen van Klembordbeheer..."

#: ../panel-plugin/plugin.c:321
msgid "Contributors:"
msgstr "Bijdragers:"

#: ../panel-plugin/plugin.c:335
msgid "Clipboard Manager for Xfce"
msgstr "Klembordbeheer voor Xfce"

#: ../panel-plugin/plugin.c:343
msgid "translator-credits"
msgstr "Vincent Tunru\nPjotr <pjotrvertaalt@gmail.com>"

#: ../panel-plugin/plugin.c:357
msgid "Unable to open the settings dialog"
msgstr "Kan het instellingenvenster niet openen"

#: ../panel-plugin/common.c:29
msgid ""
"Could not start the Clipboard Manager Daemon because it is already running."
msgstr "Kon de achtergronddienst voor Klembordbeheer niet starten omdat die reeds draait."

#: ../panel-plugin/common.c:30
msgid "The Xfce Clipboard Manager is already running."
msgstr "Het Klembordbeheer van Xfce draait reeds."

#: ../panel-plugin/common.c:37
msgid "You can launch it with 'xfce4-clipman'."
msgstr "U kunt het starten met 'xfce4-clipman'."

#: ../panel-plugin/common.c:39
msgid "The Clipboard Manager Daemon is not running."
msgstr "De achtergronddienst voor Klembordbeheer draait niet."

#: ../panel-plugin/xfce4-clipman-history.c:249
msgid "Enter search phrase here"
msgstr "Voer zoekterm hier in"

#: ../panel-plugin/xfce4-clipman-history.c:423
#, c-format
msgid "_Paste"
msgstr "_Plakken"

#: ../panel-plugin/xfce4-clipman-history.c:428
#, c-format
msgid "_Copy"
msgstr "_Kopiëren"

#: ../panel-plugin/xfce4-clipman-history.c:444
msgid "Clipman History"
msgstr "Geschiedenis van Klembordbeheer"

#: ../panel-plugin/xfce4-clipman-history.c:458
#: ../panel-plugin/xfce4-clipman-history.c:460
msgid "_Settings"
msgstr "_Instellingen"

#: ../panel-plugin/xfce4-clipman-settings.desktop.in.h:1
msgid "Clipboard Manager Settings"
msgstr "Instellingen van Klembordbeheer"

#: ../panel-plugin/xfce4-clipman-settings.desktop.in.h:2
msgid "Customize your clipboard"
msgstr "Pas uw klembord aan"
